package com.example.marco.conf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


@SuppressWarnings("HardCodedStringLiteral")
public class Session {

    @SerializedName("description")
    private String description;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("speaker")
    private String speaker;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("title")
    private String title;
    @SerializedName("tracks")
    private ArrayList<String> tracks;
    @SerializedName("uid")
    private String uid;

    public Session() {
    }

    public Session(String description, String endTime, String speaker, String startTime, String title, ArrayList<String> tracks, String uid) {
        this.description = description;
        this.endTime = endTime;
        this.speaker = speaker;
        this.startTime = startTime;
        this.title = title;
        this.tracks = tracks;
        this.uid = uid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<String> tracks) {
        this.tracks = tracks;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSessionStartHour() {
        return startTime.split("T")[1];
    }

    public String getSessionDate() {
        return startTime.split("T")[0];
    }

    public String getSessionEndHour() {
        return endTime.split("T")[1];
    }

}
