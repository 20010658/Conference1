package com.example.marco.conf.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marco.conf.MainActivity;
import com.example.marco.conf.R;
import com.example.marco.conf.Singleton;
import com.example.marco.conf.Util;
import com.example.marco.conf.adapters.SessionsAdapter;
import com.example.marco.conf.models.Session;

import java.util.ArrayList;



public class SessionsFragment extends Fragment {
    private SessionsAdapter sAdapter;
    private ArrayList<Session> sessions;
    private ArrayList<Session> sessionsDay1;
    private ArrayList<Session> sessionsDay2;
    private ArrayList<Session> sessionsDay3;

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.sessions_title));

        Singleton.setCurrentFragment("sessions");

        // Defines the xml file for the fragment
        View fragmentView = inflater.inflate(R.layout.fragment_recycler_view, parent, false);
        //RecyclerView
        RecyclerView rvSessions = (RecyclerView) fragmentView.findViewById(R.id.rvFragment);
        RecyclerView.LayoutManager tLayoutManager = new LinearLayoutManager(parent.getContext());
        rvSessions.setLayoutManager(tLayoutManager);
        rvSessions.setItemAnimator(new DefaultItemAnimator());
        sessions = new ArrayList<>(Singleton.getConference().getSessions());
        sessionsDay1 = new ArrayList<>();
        sessionsDay2 = new ArrayList<>();
        sessionsDay3 = new ArrayList<>();
        sortSessions();
        if (((MainActivity) getContext()).getTrackNumber() == 0)
            sAdapter = new SessionsAdapter(getContext(), sessions);
        else if (((MainActivity) getContext()).getTrackNumber() == 1)
            sAdapter = new SessionsAdapter(getContext(), sessionsDay1);
        else if (((MainActivity) getContext()).getTrackNumber() == 2)
            sAdapter = new SessionsAdapter(getContext(), sessionsDay2);
        else if (((MainActivity) getContext()).getTrackNumber() == 3)
            sAdapter = new SessionsAdapter(getContext(), sessionsDay3);
        rvSessions.setAdapter(sAdapter);
        return fragmentView;
    }

    /**
     * Method that orders the whole Session array in the first place.
     * After it cycles on every item of the array in order to create three different arrays, one for each day (track).
     * Done this it's possible to filter the sessions on daily basis.
     */
    private void sortSessions() {

        Util.sorter(sessions);
        for (Session s : sessions) {
            if (s.getSessionDate().contains("-10-27")) {
                sessionsDay1.add(s);
            } else if (s.getSessionDate().contains("-10-28")) {
                sessionsDay2.add(s);
            } else if (s.getSessionDate().contains("-10-29")) {
                sessionsDay3.add(s);
            }
        }
        Util.sorter(sessionsDay1);
        Util.sorter(sessionsDay2);
        Util.sorter(sessionsDay3);
        sessions.clear();
        sessions.addAll(sessionsDay1);
        sessions.addAll(sessionsDay2);
        sessions.addAll(sessionsDay3);
    }
}
