package com.example.marco.conf.adapters;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.marco.conf.models.Session;
import com.example.marco.conf.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SessionDetailedAdapter extends RecyclerView.Adapter<SessionDetailedAdapter.ViewHolder> {

    // Constant
    private static final String TAG = "SessionDetailedAdapter";

    // Fields
    private final Context mContext;
    private final List<Session> mSessions;

    /**
     * Instantiates a new Session detailed adapter.
     *
     * @param context  the context
     * @param sessions the sessions
     */
    public SessionDetailedAdapter(Context context, List<Session> sessions) {
        mContext = context;
        mSessions = sessions;
    }

    @Override
    public SessionDetailedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View sessionsDetailedView = inflater.inflate(R.layout.session_detailed_item, parent, false);
        return new ViewHolder(sessionsDetailedView);
    }

    @Override
    public void onBindViewHolder(SessionDetailedAdapter.ViewHolder holder, int position) {
        final Session session = mSessions.get(position);
        final TextView detailedTextViewTitle = holder.titleTextView;
        detailedTextViewTitle.setText(String.valueOf(session.getTitle()));
        final TextView detailedTextViewDescription = holder.descriptionTextView;
        if (!session.getDescription().isEmpty())
            detailedTextViewDescription.setText(session.getDescription());
        TextView detailedTextViewSpeaker = holder.speakersTextView;
        if (!session.getSpeaker().isEmpty())
            detailedTextViewSpeaker.setText(session.getSpeaker());
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ITALY);
        TextView detailedTextViewStart = holder.startTextView;
        TextView detailedTextViewEnd = holder.endTextView;
        try {
            detailedTextViewStart.setText(sdf.parse(session.getStartTime()).toString().replace("GMT+01:00", ""));
            detailedTextViewEnd.setText(sdf.parse(session.getEndTime()).toString().replace("GMT+01:00", ""));
        } catch (ParseException e) {
            Log.e(TAG, "Error while parsing start/end date: ", e);
        }
        Button addEventButton = holder.addToCalButton;
        addEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");
                calIntent.putExtra(CalendarContract.Events.TITLE, detailedTextViewTitle.getText());
                calIntent.putExtra(CalendarContract.Events.DESCRIPTION, detailedTextViewDescription.getText());
                Calendar calStart = Calendar.getInstance();
                Calendar calEnd = Calendar.getInstance();
                try {
                    calStart.setTime(sdf.parse(session.getStartTime()));
                    calEnd.setTime(sdf.parse(session.getEndTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calStart.getTimeInMillis());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calEnd.getTimeInMillis());
                mContext.startActivity(calIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSessions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView titleTextView;
        final TextView descriptionTextView;
        final TextView speakersTextView;
        final TextView startTextView;
        final TextView endTextView;
        final Button addToCalButton;

        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.session_title);
            descriptionTextView = (TextView) itemView.findViewById(R.id.session_description);
            speakersTextView = (TextView) itemView.findViewById(R.id.session_speaker);
            startTextView = (TextView) itemView.findViewById(R.id.session_s_hour);
            endTextView = (TextView) itemView.findViewById(R.id.session_e_hour);
            addToCalButton = (Button) itemView.findViewById(R.id.add_session_calendar);
        }
    }


}
