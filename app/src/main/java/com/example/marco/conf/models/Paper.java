package com.example.marco.conf.models;

import com.google.firebase.database.PropertyName;


class Paper {
    @PropertyName("abstract")
    private String abstract1;
    private int id;
    private String keywords;
    private String manuscript_download_url;
    private String manuscript_filename;
    private String manuscript_type;
    private String title;
    private String uid;

}
