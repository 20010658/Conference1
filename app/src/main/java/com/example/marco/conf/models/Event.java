package com.example.marco.conf.models;

import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.SerializedName;


public class Event {

    @SerializedName("location")
    @PropertyName("location")
    private EventLocation location;
    @SerializedName("id")
    @PropertyName("id")
    private int id;
    @SerializedName("uid")
    @PropertyName("uid")
    private String uid;
    @SerializedName("title")
    @PropertyName("title")
    private String title;
    @SerializedName("url")
    @PropertyName("url")
    private String url;
    @SerializedName("logo")
    @PropertyName("logo")
    private String logo;
    @SerializedName("description")
    @PropertyName("description")
    private String description;
    @SerializedName("created")
    @PropertyName("created")
    private String created;
    @PropertyName("startDate")
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("venue")
    @PropertyName("venue")
    private String venue;
    @SerializedName("endDate")
    @PropertyName("endDate")
    private String endDate;
    @SerializedName("status")
    @PropertyName("status")
    private int status;
    @SerializedName("lisperatorId")
    @PropertyName("lisperatorId")
    private int lisperatorId;

    public Event() {
    }

    public Event(EventLocation location, int eventID, String eventUID, String eventTitle, String eventURL, String eventLogo, String eventDescription, String eventCreated, String eventStartDate, String eventVenue, String eventEndDate, int eventStatus, int eventLisperatorId) {
        this.location = location;
        this.id = eventID;
        this.uid = eventUID;
        this.title = eventTitle;
        this.url = eventURL;
        this.logo = eventLogo;
        this.description = eventDescription;
        this.created = eventCreated;
        this.startDate = eventStartDate;
        this.venue = eventVenue;
        this.endDate = eventEndDate;
        this.status = eventStatus;
        this.lisperatorId = eventLisperatorId;
    }

    public EventLocation getLocation() {
        return location;
    }

    public void setLocation(EventLocation location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getLisperatorId() {
        return lisperatorId;
    }

    public void setLisperatorId(int lisperatorId) {
        this.lisperatorId = lisperatorId;
    }
}


