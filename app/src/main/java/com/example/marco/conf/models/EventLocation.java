package com.example.marco.conf.models;

import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.SerializedName;


public class EventLocation {

    @SerializedName("longitude")
    @PropertyName("longitude")
    private String longitude;
    @SerializedName("latitude")
    @PropertyName("latitude")
    private String latitude;
    @SerializedName("label")
    @PropertyName("label")
    private String label;

    public EventLocation() {
    }

    public EventLocation(String eventLongitude, String eventLatitude, String eventLabel) {
        this.longitude = eventLongitude;
        this.latitude = eventLatitude;
        this.label = eventLabel;
    }

    public String getEventLongitude() {
        return longitude;
    }

    public void setEventLongitude(String eventLongitude) {
        this.longitude = eventLongitude;
    }

    public String getEventLatitude() {
        return latitude;
    }

    public void setEventLatitude(String eventLatitude) {
        this.latitude = eventLatitude;
    }

    public String getEventLabel() {
        return label;
    }

    public void setEventLabel(String eventLabel) {
        this.label = eventLabel;
    }
}
