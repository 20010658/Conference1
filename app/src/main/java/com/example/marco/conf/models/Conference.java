package com.example.marco.conf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Conference {
    @SerializedName("event")
    private Event event;
    @SerializedName("tracks")
    private ArrayList<Track> tracks;
    @SerializedName("sessions")
    private ArrayList<Session> sessions;

    public Conference() {
    }

    public Conference(Event event, ArrayList<Track> tracks, ArrayList<Session> sessions) {
        this.event = event;
        this.tracks = tracks;
        this.sessions = sessions;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public ArrayList<Track> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<Track> tracks) {
        this.tracks = tracks;
    }

    public ArrayList<Session> getSessions() {
        return sessions;
    }

    public void setSessions(ArrayList<Session> sessions) {
        this.sessions = sessions;
    }
}
