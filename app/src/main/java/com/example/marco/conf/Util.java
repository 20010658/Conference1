package com.example.marco.conf;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.example.marco.conf.models.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static android.app.Notification.DEFAULT_VIBRATE;



public class Util {

    /**
     * Given an arraylist of sessions it sorts looking for days and hours
     *
     * @param aList sorted arraylist
     */
    public static void sorter(ArrayList<Session> aList) {
        Collections.sort(aList, new Comparator<Session>() {
            @Override
            public int compare(Session lhs, Session rhs) {
                return lhs.getSessionStartHour().compareTo(rhs.getSessionStartHour());
            }
        });
    }

    static void notifyUser() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(Singleton.getContext())
                        .setSmallIcon(R.drawable.graduationcap36)
                        .setDefaults(DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentTitle(Singleton.getContext().getString(R.string.notify_title))
                        .setContentText(Singleton.getContext().getString(R.string.notify_text));
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(Singleton.getContext(), MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(Singleton.getContext());
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) Singleton.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // mNotificationId is a unique integer your app uses to identify the
        // notification. For example, to cancel the notification, you can pass its ID
        // number to NotificationManager.cancel().

        mNotificationManager.notify(9666, mBuilder.build());
    }

    /* This method shows a dialog message to the user. */
    public static void showDialogMessage(String title, String message, Context ctx) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(ctx, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(ctx);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
