package com.example.marco.conf;

import android.content.Context;

import com.example.marco.conf.models.Conference;

public class Singleton {

    static Conference conference = new Conference();
    private static Context context;
    private static String currentFragment;

    // .Ctor
    Singleton(Context context) {
        Singleton.context = context;
    }


    public static Conference getConference() {
        return conference;
    }

    public static Context getContext() {
        return context;
    }

    public static String getCurrentFragment() {
        return currentFragment;
    }

    public static void setCurrentFragment(String currentFragment) {
        Singleton.currentFragment = currentFragment;
    }
}
