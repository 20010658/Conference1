package com.example.marco.conf.models;

import com.google.gson.annotations.SerializedName;

public class Track {

    @SerializedName("description")
    private String description;
    @SerializedName("event")
    private String event;
    @SerializedName("title")
    private String title;
    @SerializedName("uid")
    private String uid;

    public Track() {
    }

    public Track(String description, String event, String title, String uid) {
        this.description = description;
        this.event = event;
        this.title = title;
        this.uid = uid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
