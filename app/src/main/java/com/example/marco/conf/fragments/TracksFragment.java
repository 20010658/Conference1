package com.example.marco.conf.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marco.conf.R;
import com.example.marco.conf.Singleton;
import com.example.marco.conf.adapters.TracksAdapter;
import com.example.marco.conf.models.Track;

import java.util.ArrayList;


public class TracksFragment extends Fragment {

    private ArrayList<Track> tracks;

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.tracks_title));

        Singleton.setCurrentFragment("tracks");

        // Defines the xml file for the fragment
        View fragmentView = inflater.inflate(R.layout.fragment_recycler_view, parent, false);
        //RecyclerView
        RecyclerView rvTracks = (RecyclerView) fragmentView.findViewById(R.id.rvFragment);
        RecyclerView.LayoutManager tLayoutManager = new LinearLayoutManager(parent.getContext());
        rvTracks.setLayoutManager(tLayoutManager);
        rvTracks.setItemAnimator(new DefaultItemAnimator());
        tracks = new ArrayList<>(Singleton.getConference().getTracks());
        sortTracks();
        TracksAdapter tAdapter = new TracksAdapter(getContext(), tracks);
        rvTracks.setAdapter(tAdapter);
        return fragmentView;
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    }

    /**
     * Method that sorts a given track array.
     */
    private void sortTracks() {
        Track temp;
        for (int i = 0; i < tracks.size(); i++) {
            for (int j = i; j < tracks.size(); j++) {
                if (Integer.parseInt(tracks.get(i).getTitle().split(" ")[1]) > Integer.parseInt(tracks.get(j).getTitle().split(" ")[1])) {
                    temp = tracks.get(j);
                    tracks.set(j, tracks.get(i));
                    tracks.set(i, temp);
                }
            }
        }
    }
}
