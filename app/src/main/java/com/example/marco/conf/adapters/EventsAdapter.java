package com.example.marco.conf.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.marco.conf.MainActivity;
import com.example.marco.conf.R;
import com.example.marco.conf.fragments.TracksFragment;
import com.example.marco.conf.models.Event;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private final List<Event> mEvent;
    private final Context mContext;

    /**
     * Instantiates a new Events adapter.
     *
     * @param context the context
     * @param events  the events
     */
    public EventsAdapter(Context context, List<Event> events) {
        mEvent = events;
        mContext = context;
    }

    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View eventsView = inflater.inflate(R.layout.event_item, parent, false);
        return new ViewHolder(eventsView);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {
        Event event = mEvent.get(position);
        TextView textViewTitle = holder.titleTextView;
        textViewTitle.setText(event.getTitle());
        TextView textViewUrl = holder.urlTextView;
        textViewUrl.setText(event.getUrl());
        TextView textViewDescription = holder.descriptionTextView;
        textViewDescription.setText(event.getDescription());
        TextView textViewVenue = holder.venueTextView;
        textViewVenue.setText(event.getVenue());
        Button seeTrackButton = holder.seeTrackButton;
        seeTrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TracksFragment tracksFragment = new TracksFragment();
                ((MainActivity) mContext).addFragment(tracksFragment);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mEvent.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView titleTextView;
        final TextView descriptionTextView;
        final TextView urlTextView;
        final TextView venueTextView;
        final Button seeTrackButton;

        ViewHolder(final View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.event_title);
            urlTextView = (TextView) itemView.findViewById(R.id.event_url);
            descriptionTextView = (TextView) itemView.findViewById(R.id.event_description);
            venueTextView = (TextView) itemView.findViewById(R.id.event_venue);
            seeTrackButton = (Button) itemView.findViewById(R.id.see_track_button);
        }
    }
}
