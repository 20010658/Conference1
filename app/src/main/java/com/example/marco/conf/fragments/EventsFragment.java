package com.example.marco.conf.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marco.conf.R;
import com.example.marco.conf.Singleton;
import com.example.marco.conf.adapters.EventsAdapter;
import com.example.marco.conf.models.Event;

import java.util.ArrayList;


public class EventsFragment extends Fragment {

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.events_title));

        Singleton.setCurrentFragment("events");

        // Defines the xml file for the fragment
        View fragmentView = inflater.inflate(R.layout.fragment_recycler_view, parent, false);
        //RecyclerView
        RecyclerView rvEvents = (RecyclerView) fragmentView.findViewById(R.id.rvFragment);
        RecyclerView.LayoutManager tLayoutManager = new LinearLayoutManager(parent.getContext());
        rvEvents.setLayoutManager(tLayoutManager);
        rvEvents.setItemAnimator(new DefaultItemAnimator());
        ArrayList<Event> events = new ArrayList<>();
        events.add(Singleton.getConference().getEvent());
        EventsAdapter eAdapter = new EventsAdapter(getContext(), events);
        rvEvents.setAdapter(eAdapter);
        return fragmentView;
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
    }

}
