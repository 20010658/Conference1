package com.example.marco.conf.adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.marco.conf.MainActivity;
import com.example.marco.conf.models.Session;
import com.example.marco.conf.R;

import java.util.List;

public class SessionsAdapter extends RecyclerView.Adapter<SessionsAdapter.ViewHolder> {

    private final List<Session> mSessions;
    private final Context mContext;

    /**
     * Instantiates a new Sessions adapter.
     *
     * @param context  the context
     * @param sessions the sessions
     */
    public SessionsAdapter(Context context, List<Session> sessions) {
        mSessions = sessions;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public SessionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View sessionsView = inflater.inflate(R.layout.sessions_item, parent, false);
        return new ViewHolder(sessionsView);
    }

    @Override
    public void onBindViewHolder(SessionsAdapter.ViewHolder holder, int position) {
        final Session session = mSessions.get(position);
        final TextView textViewTitle = holder.titleTextView;
        textViewTitle.setText(session.getTitle());
        final TextView textViewDescription = holder.descriptionTextView;
        if (!session.getDescription().equals(""))
            textViewDescription.setText(session.getDescription());
        Button buttonDetails = holder.seeDetailsSession;
        buttonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getContext()).addDetailedSessionsFragment(session);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSessions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        final TextView titleTextView;
        final TextView descriptionTextView;
        final Button seeDetailsSession;

        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.session_title);
            descriptionTextView = (TextView) itemView.findViewById(R.id.session_description);
            seeDetailsSession = (Button) itemView.findViewById(R.id.see_details_session);
        }
    }
}
