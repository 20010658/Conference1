package com.example.marco.conf.models;


class PaperAuthor {

    private String affiliation;
    private String email;
    private String first_name;
    private int id;
    private String last_name;
    private String uid;
}
