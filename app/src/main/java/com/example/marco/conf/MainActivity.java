package com.example.marco.conf;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.marco.conf.fragments.EventsFragment;
import com.example.marco.conf.fragments.SessionDetailedFragment;
import com.example.marco.conf.fragments.SessionsFragment;
import com.example.marco.conf.fragments.TracksFragment;
import com.example.marco.conf.models.Conference;
import com.example.marco.conf.models.Session;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;




public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Constant
    private static final String TAG = "MainActivity";

    private Toolbar toolbar;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private int trackNumber = 0;
    private EventsFragment eventsFragment;
    private TracksFragment tracksFragment;
    private SessionsFragment sessionsFragment;
    private boolean firstOpen;


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        parserChooser();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        //noinspection deprecation
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        Singleton singleton = new Singleton(getApplicationContext());
        firstOpen = true;
    }

    @Override
    public void onBackPressed() {
        // Handle back button clicks here
        Log.i(TAG, "Back pressed");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (eventsFragment.isVisible() && !drawer.isDrawerOpen(GravityCompat.START))
                drawer.openDrawer(GravityCompat.START);
            else if (Singleton.getCurrentFragment().equals("tracks")) {
                eventsFragment = new EventsFragment();
                addFragment(eventsFragment);
            } else if (Singleton.getCurrentFragment().equals("sessions")) {
                tracksFragment = new TracksFragment();
                addFragment(tracksFragment);
            } else if (Singleton.getCurrentFragment().equals("sessionsDetailed")) {
                sessionsFragment = new SessionsFragment();
                addFragment(sessionsFragment);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.draw_tracks:
                tracksFragment = new TracksFragment();
                addFragment(tracksFragment);
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.draw_sessions:
                setTrackNumber(0);
                sessionsFragment = new SessionsFragment();
                addFragment(sessionsFragment);
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.draw_events:
                eventsFragment = new EventsFragment();
                addFragment(eventsFragment);
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.parser_button:
                firstOpen = true;
                if (getFragmentManager().findFragmentById(R.id.your_placeholder) != null)
                    getFragmentManager().beginTransaction()
                            .remove(getFragmentManager().findFragmentById(R.id.your_placeholder))
                            .commit();
                parserChooser();
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                firstOpen = true;
                return true;
        }
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addFragment(Fragment fragment) {
        // Fragment Manager
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.your_placeholder, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Shows the details for the chosen Session adding a new fragment.
     *
     * @param s the chosen session
     */
    public void addDetailedSessionsFragment(Session s) {
        // Fragment Manager
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        // Animate fragment
        SessionDetailedFragment sessionDetailedFragment = new SessionDetailedFragment();
        fragmentTransaction.replace(R.id.your_placeholder, sessionDetailedFragment);
        sessionDetailedFragment.setFragmentObject(s);
        fragmentTransaction.commit();
    }

    /**
     * Sets the action bar title according to the displayed fragment
     *
     * @param title action bar title to display
     */
    @SuppressWarnings("unused")
    public void setActionBarTitle(String title) {
        toolbar.setTitle(title);
    }

    /**
     * Returns the track number
     *
     * @return trackNumber
     */
    public int getTrackNumber() {
        return trackNumber;
    }

    /**
     * Sets the value of the variable "trackNumber" in order to easily show only the sessions of the chosen track
     *
     * @param n the chosen track number
     */
    public void setTrackNumber(int n) {
        trackNumber = n;
    }

    /**
     * Method that creates a dialog with two buttons in order to choose where to retrieve data.
     * It also handles the button clicks, according to the return value of isOnline() method.
     */
    private void parserChooser() {
        @SuppressLint("InflateParams") View addView = getLayoutInflater().inflate(R.layout.choose_parser_dialog, null);
        Button jsonButton = (Button) addView.findViewById(R.id.json_button);
        final Button rtdbButton = (Button) addView.findViewById(R.id.rtdb_button);
        final AlertDialog alertD = new AlertDialog.Builder(this).setCancelable(false).setView(addView).show();
        assert jsonButton != null;
        jsonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new JsonParser().execute();
                alertD.dismiss();
            }
        });
        if (isOnline()) {
            rtdbButton.setEnabled(true);
            rtdbButton.setTextColor(getColor(R.color.colorAccentApp));
            rtdbButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isOnline()) {
                        new FirebaseConferenceTask().execute();
                        alertD.dismiss();
                    } else {
                        Util.showDialogMessage(getString(R.string.warning), getString(R.string.deviceOffline), MainActivity.this);
                        rtdbButton.setEnabled(false);
                        rtdbButton.setTextColor(getColor(R.color.disabledButton));
                    }
                }
            });
        } else {
            rtdbButton.setEnabled(false);
            rtdbButton.setTextColor(getColor(R.color.disabledButton));
            Toast.makeText(this, R.string.toast_not_connected, Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that checks if the smartphone is online
     *
     * @return boolean value
     */
    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    /**
     * AsyncTask that retrieves data from the local json file.
     * It's asyncronous because the execution could freeze the current working activity and UI.
     */
    private class JsonParser extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), R.string.toast_json_parser, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                InputStream file = getAssets().open(getString(R.string.json_file_name));
                int size = file.available();
                byte[] buffer = new byte[size];
                //noinspection ResultOfMethodCallIgnored
                file.read(buffer);
                file.close();
                String json = new String(buffer, "UTF-8");
                Gson gson = new GsonBuilder().create();
                Type type = new TypeToken<Conference>() {
                }.getType();
                Singleton.conference = gson.fromJson(json, type);

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            eventsFragment = new EventsFragment();
            addFragment(eventsFragment);
        }


    }

    /**
     * AsyncTask that retrieves data from the realtime database.
     * It's asyncronous because the execution could freeze the current working activity and UI.
     */
    private class FirebaseConferenceTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), R.string.toast_rtdb_parser, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            // Get the FirebaseDatabase instance
            // And get the database reference too
            DatabaseReference conferenceRef = FirebaseDatabase.getInstance().getReference();

            conferenceRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Singleton.conference = dataSnapshot.getValue(Conference.class);
                    eventsFragment = new EventsFragment();
                    addFragment(eventsFragment);
                    assert Singleton.conference != null;
                    if (!firstOpen) {
                        Util.notifyUser();
                    } else {
                        firstOpen = false;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Failed to read value
                    Log.e(TAG, "Error while reading Firebase DB: " + databaseError.getDetails());
                }
            });

            return null;
        }

    }
}

