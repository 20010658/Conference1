package com.example.marco.conf.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.marco.conf.MainActivity;
import com.example.marco.conf.fragments.SessionsFragment;
import com.example.marco.conf.models.Track;
import com.example.marco.conf.R;

import java.util.List;

public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.ViewHolder> {

    private final List<Track> mTracks;
    private final Context mContext;

    /**
     * Instantiates a new Tracks adapter.
     *
     * @param context the context
     * @param tracks  the tracks
     */
    public TracksAdapter(Context context, List<Track> tracks) {
        mTracks = tracks;
        mContext = context;
    }

    @Override
    public TracksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View tracksView = inflater.inflate(R.layout.tracks_item, parent, false);
        return new ViewHolder(tracksView);
    }

    @Override
    public void onBindViewHolder(TracksAdapter.ViewHolder holder, int position) {
        Track track = mTracks.get(position);
        final TextView textViewTitle = holder.titleTextView;
        textViewTitle.setText(track.getTitle());
        TextView textViewDescription = holder.descriptionTextView;
        textViewDescription.setText(track.getDescription());
        Button seeSessionsButton = holder.seeSessionsButton;
        seeSessionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) mContext).setTrackNumber(Integer.parseInt(textViewTitle.getText().toString().split(" ")[1]));
                SessionsFragment sessionsFragment = new SessionsFragment();
                ((MainActivity) mContext).addFragment(sessionsFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTracks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView titleTextView;
        final TextView descriptionTextView;
        final Button seeSessionsButton;

        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.track_title);
            descriptionTextView = (TextView) itemView.findViewById(R.id.track_description);
            seeSessionsButton = (Button) itemView.findViewById(R.id.see_sessions_button);
        }
    }

}
