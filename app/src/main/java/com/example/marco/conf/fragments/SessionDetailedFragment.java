package com.example.marco.conf.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marco.conf.R;
import com.example.marco.conf.Singleton;
import com.example.marco.conf.adapters.SessionDetailedAdapter;
import com.example.marco.conf.models.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionDetailedFragment extends Fragment {

    private final List<Session> detailedSession = new ArrayList<>();
    private Session session;

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        getActivity().setTitle(session.getTitle());

        Singleton.setCurrentFragment("sessionsDetailed");

        SessionDetailedAdapter sDetailedAdapter = new SessionDetailedAdapter(getContext(), detailedSession);
        // Defines the xml file for the fragment
        View fragmentView = inflater.inflate(R.layout.fragment_recycler_view, parent, false);
        //RecyclerView
        RecyclerView rvDetailedSession = (RecyclerView) fragmentView.findViewById(R.id.rvFragment);
        RecyclerView.LayoutManager tLayoutManager = new LinearLayoutManager(parent.getContext());
        rvDetailedSession.setLayoutManager(tLayoutManager);
        rvDetailedSession.setItemAnimator(new DefaultItemAnimator());
        detailedSession.add(session);
        rvDetailedSession.setAdapter(sDetailedAdapter);
        return fragmentView;
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

    }

    /**
     * Method that pass the Session object to the fragment.
     *
     * @param ses the Session object
     */
    public void setFragmentObject(Session ses) {
        session = ses;
    }

}
